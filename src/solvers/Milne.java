package solvers;

import equations.Equation;

import java.util.function.DoubleUnaryOperator;

public class Milne implements Solver {
    private final Equation equation;
    private final DoubleUnaryOperator function;
    private final double from;
    private final double to;
    private final double y;
    private final double step;
    private final double epsilon;

    public Milne(Equation equation, DoubleUnaryOperator function, double from, double to, double y, double step, double epsilon) {
        this.equation = equation;
        this.function = function;
        this.from = from;
        this.to = to;
        this.y = y;
        this.step = step;
        this.epsilon = epsilon;
    }

    public String getTitle() {
        return "Метод Милна";
    }

    public double[][] solve() {
        double[][] points = new double[(int) (Math.floor(to - from) / step) + 1][2];
        double[] f = new double[points.length];
        points[0][0] = from;
        points[0][1] = y;
        f[0] = equation.f(from, y);

        for (int i = 0; i < 4 && i < points.length; i++) {
            double k1 = step * equation.f(points[i][0], points[i][1]);
            double k2 = step * equation.f(points[i][0] + step * 0.5, points[i][1] + k1 * 0.5);
            double k3 = step * equation.f(points[i][0] + step * 0.5, points[i][1] + k2 * 0.5);
            double k4 = step * equation.f(points[i][0] + step, points[i][1] + k3);

            points[i + 1][0] = points[i][0] + step;
            points[i + 1][1] = points[i][1] + (k1 + 2 * k2 + 2 * k3 + k4) / 6;
            f[i + 1] = equation.f(points[i + 1][0], points[i + 1][1]);
        }

        for (int i = 4; i < points.length; i++) {
            points[i][0] = points[i - 1][0] + step;
            points[i][1] = points[i - 4][1] + 4 * step / 3 * (2 * f[i - 3] - f[i - 2] + 2 * f[i - 1]);
            f[i] = equation.f(points[i][0], points[i][1]);
            double answer = function.applyAsDouble(points[i][0]);

            do {
                points[i][1] = points[i - 2][1] + step / 3 * (f[i - 2] + 4 * f[i - 1] + f[i]);
                f[i] = equation.f(points[i][0], points[i][1]);
            } while (Math.abs(points[i][1] - answer) / 15 > epsilon);
        }

        return points;
    }
}
