package solvers;

import equations.Equation;

public class EulerImproved implements Solver {
    private final Equation equation;
    private final double from;
    private final double to;
    private final double y;
    private final double step;
    private final double epsilon;

    public EulerImproved(Equation equation, double from, double to, double y, double step, double epsilon) {
        this.equation = equation;
        this.from = from;
        this.to = to;
        this.y = y;
        this.step = step;
        this.epsilon = epsilon;
    }

    public String getTitle() {
        return "Модифицированный метод Эйлера";
    }

    public double[][] solve() {
        return solve(step, epsilon);
    }

    private double[][] solve(double step, double epsilon) {
        boolean solved = true;
        double[][] points = new double[(int) (Math.floor(to - from) / step) + 1][2];
        points[0][0] = from;
        points[0][1] = y;

        for (int i = 0; i < points.length - 1; i++) {
            points[i + 1][0] = points[i][0] + step;
            double f = equation.f(points[i][0], points[i][1]);
            points[i + 1][1] = points[i][1] + 0.5 * step * (f + equation.f(points[i + 1][0], points[i][1] + step * f));

            double half = points[i][1] + 0.25 * step * (f + equation.f(points[i + 1][0], points[i][1] + 0.5 * step * f));
            if (Math.abs(points[i + 1][1] - half) / 3 > epsilon) {
                solved = false;
                break;
            }
        }

        if (solved)
            return points;
        else
            return solve(step * 0.5, epsilon);
    }
}
