package solvers;

public interface Solver {
    double[][] solve();
    String getTitle();
}
