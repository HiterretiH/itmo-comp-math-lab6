import equations.Equation;
import solvers.Euler;
import solvers.EulerImproved;
import solvers.Milne;
import solvers.Solver;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.function.DoubleUnaryOperator;

public class Application {
    private final JFrame frame;

    public Application(List<Equation> equations) {
        frame = new JFrame("Выбор функции");

        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        frame.getContentPane().setBackground(Color.WHITE);

        Font font = new Font("Arial", Font.PLAIN, 24);

        JLabel fromLabel = new JLabel("<html>x<sub>0</sub>: </html>");
        JLabel toLabel = new JLabel("<html>x<sub>n</sub>: </html>");
        JLabel yLabel = new JLabel("<html>Значение функции в x<sub>0</sub>: </html>");
        JLabel stepLabel = new JLabel("Шаг h: ");
        JLabel epsilonLabel = new JLabel("Погрешность ε: ");

        JTextField fromField = new JTextField("1");
        JTextField toField = new JTextField("5");
        JTextField yField = new JTextField("1");
        JTextField stepField = new JTextField("0.25");
        JTextField epsilonField = new JTextField("0.1");

        JLabel equationsLabel = new JLabel("Выберите функцию:");

        frame.add(fromLabel);
        frame.add(fromField);

        frame.add(toLabel);
        frame.add(toField);

        frame.add(yLabel);
        frame.add(yField);

        frame.add(stepLabel);
        frame.add(stepField);

        frame.add(epsilonLabel);
        frame.add(epsilonField);
        frame.add(Box.createVerticalStrut(5));

        frame.add(equationsLabel);
        frame.add(Box.createVerticalStrut(5));

        for (Equation equation : equations) {
            JButton button = new JButton(equation.getAsText());
            button.setFont(font);
            button.setBackground(null);

            button.addActionListener(e -> submit(
                    fromField.getText(),
                    toField.getText(),
                    yField.getText(),
                    stepField.getText(),
                    epsilonField.getText(),
                    equation
            ));

            frame.add(button);
        }

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        frame.setMinimumSize(new Dimension(200, 300));
        frame.setLocationRelativeTo(null);
    }

    private void submit(String fromText, String toText, String yText, String stepText, String epsilonText, Equation equation) {
        double from, to, step, epsilon, y;

        try {
            from = Double.parseDouble(fromText);
            to = Double.parseDouble(toText);
        }
        catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(frame, "Значения x должны быть числами", "", JOptionPane.ERROR_MESSAGE);
            return;
        }

        try {
            y = Double.parseDouble(yText);
        }
        catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(frame, "Значение y должно быть числом", "", JOptionPane.ERROR_MESSAGE);
            return;
        }

        try {
            step = Double.parseDouble(stepText);
        }
        catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(frame, "Значение шага должно быть числом", "", JOptionPane.ERROR_MESSAGE);
            return;
        }

        try {
            epsilon = Double.parseDouble(epsilonText);
        }
        catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(frame, "Значение погрешности должно быть числом", "", JOptionPane.ERROR_MESSAGE);
            return;
        }

        DoubleUnaryOperator function = equation.getRealFunction(from, y);

        new FunctionWindow(equation, function, from, to, new Solver[] {
                new Euler(equation, from, to, y, step, epsilon),
                new EulerImproved(equation, from, to, y, step, epsilon),
                new Milne(equation, function, from, to, y, step, epsilon)
        });
    }
}