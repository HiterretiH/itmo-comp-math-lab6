package util;

public class NumberFormatter {
    public static String format(double a) {
        String result = Double.toString(a);
        if (result.indexOf('E') == -1) {
            return result;
        }
        return "<html>" + result.replace("E", "*10<sup>") + "</sup></html>";
    }
}
