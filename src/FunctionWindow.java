import equations.Equation;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.LegendItemEntity;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import solvers.Solver;
import util.NumberFormatter;

import javax.swing.*;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.util.function.DoubleUnaryOperator;

public class FunctionWindow {
    private final int WIDTH = 1000;
    private final int TABLE_HEIGHT = 300;
    private final JFrame frame;
    private final double from;
    private final double to;
    private double min;
    private double max;


    public FunctionWindow(Equation equation, DoubleUnaryOperator function, double from, double to, Solver[] solvers) {
        frame = new JFrame("Результат");

        this.from = from;
        this.to = to;

        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        frame.getContentPane().setBackground(Color.WHITE);

        XYSeriesCollection dataset = new XYSeriesCollection();
        findRange(function);
        dataset.addSeries(getSeries(function, "Решение"));
        for (Solver solver : solvers) {
            double[][] points = solver.solve();
            dataset.addSeries(getSeries(points, solver.getTitle()));
            JLabel label = new JLabel(solver.getTitle() + ": ", JLabel.CENTER);
            label.setAlignmentX(JLabel.CENTER_ALIGNMENT);
            frame.add(label);
            frame.add(getTable(points));
        }

        ChartPanel chart = generateChart(dataset, equation.getAsText());
        frame.add(chart);

        frame.getContentPane().setBackground(Color.WHITE);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
    }

    private JScrollPane getTable(double[][] points) {
        int numColumns = points.length + 1;
        String[][] data = new String[2][numColumns];
        String[] columnNames = new String[numColumns];

        data[0][0] = "x";
        data[1][0] = "y";
        columnNames[0] = "0";

        for (int i = 0; i < points.length; i++) {
            columnNames[i + 1] = "Column " + i;
            for (int j = 0; j < 2; j++) {
                data[j][i + 1] = NumberFormatter.format(points[i][j]);
            }
        }

        JTable table = new JTable(data, columnNames);
        table.setTableHeader(null);

        int preferredWidth = 100;
        for (int i = 1; i < numColumns; i++) {
            TableColumn column = table.getColumnModel().getColumn(i);
            column.setMinWidth(preferredWidth);
        }

        table.setPreferredScrollableViewportSize(new Dimension(WIDTH, table.getRowHeight() * 2));
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        JScrollPane scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setMaximumSize(new Dimension(WIDTH, TABLE_HEIGHT));
        scrollPane.setMinimumSize(new Dimension(WIDTH, TABLE_HEIGHT));

        return scrollPane;
    }

    private void findRange(DoubleUnaryOperator function) {
        min = function.applyAsDouble(from);
        max = function.applyAsDouble(from);

        for (double x = from; x < to; x += 0.125) {
            double f = function.applyAsDouble(x);
            if (f < min) min = f;
            if (f > max) max = f;
        }

        min -= (max - min) * 0.1;
        max += (max - min) * 0.1;
    }

    private XYSeries getSeries(DoubleUnaryOperator f, String title) {
        XYSeries series = new XYSeries(title);

        double x = from;
        while (x <= to) {
            series.add(x, f.applyAsDouble(x));
            x += 0.03125;
        }

        return series;
    }

    private XYSeries getSeries(double[][] points, String title) {
        XYSeries series = new XYSeries(title);

        for (double[] point : points) {
            series.add(point[0], point[1]);
        }

        return series;
    }

    private ChartPanel generateChart(XYSeriesCollection dataset, String title) {
        Paint[] colors = new Paint[] {
                Color.RED,
                Color.GREEN,
                Color.BLUE,
                Color.CYAN,
                Color.PINK,
                Color.MAGENTA
        };

        NumberAxis xAxis = new NumberAxis("X");
        NumberAxis yAxis = new NumberAxis("Y");

        double from = this.from - (this.to - this.from) * 0.1;
        double to = this.to + (this.to - this.from) * 0.1;
        xAxis.setRange(from, to);
        yAxis.setRange(min, max);

        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer(true, true);
        renderer.setDefaultToolTipGenerator(new StandardXYToolTipGenerator());

        XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer);
        JFreeChart chart = new JFreeChart(title, plot);

        int count = dataset.getSeriesCount();
        for (int i = 0; i < count; i++) {
            renderer.setSeriesShapesVisible(i, false);
            renderer.setSeriesLinesVisible(i, true);
            renderer.setSeriesPaint(i, colors[i % colors.length]);
        }

        return getChartPanel(dataset, chart, renderer);
    }

    private ChartPanel getChartPanel(XYSeriesCollection dataset, JFreeChart chart, XYLineAndShapeRenderer renderer) {
        ChartPanel chartPanel = new ChartPanel(chart) {
            @Override
            public Dimension getPreferredSize() {
                return new Dimension(640, 480);
            }
        };

        chartPanel.setMouseWheelEnabled(true);
        chartPanel.setMouseZoomable(true, true);

        chartPanel.addChartMouseListener(new ChartMouseListener() {
            @Override
            public void chartMouseClicked(ChartMouseEvent e) {
                ChartEntity ce = e.getEntity();
                if (ce instanceof LegendItemEntity item) {
                    int index = dataset.getSeriesIndex(item.getSeriesKey());
                    boolean visible = renderer.getSeriesLinesVisible(index);

                    renderer.setSeriesLinesVisible(index, !visible);
                }
            }

            @Override
            public void chartMouseMoved(ChartMouseEvent e) {}
        });
        return chartPanel;
    }
}
