package equations;

import java.util.function.DoubleUnaryOperator;

@FunctionalInterface
public interface FunctionFinder {
    DoubleUnaryOperator findFunction(double x, double y);
}
