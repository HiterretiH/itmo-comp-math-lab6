package equations;

import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleUnaryOperator;

public class Equation {
    private final String text;
    private final DoubleBinaryOperator f;
    private final FunctionFinder functionFinder;

    public Equation(String text, DoubleBinaryOperator f, FunctionFinder functionFinder) {
        this.text = text;
        this.f = f;
        this.functionFinder = functionFinder;
    }

    public String getAsText() {
        return text;
    }

    public double f(double x, double y) {
        return f.applyAsDouble(x, y);
    }

    public DoubleUnaryOperator getRealFunction(double x, double y) {
        return functionFinder.findFunction(x, y);
    }
}
