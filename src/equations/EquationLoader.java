package equations;

import java.util.ArrayList;
import java.util.List;

public class EquationLoader {
    public static List<Equation> getFunctions() {
        List<Equation> equations = new ArrayList<>(5);

        equations.add(new Equation(
                "y' = x",
                (x, y) -> x,
                (x0, y0) -> {
                    double c = y0 - x0 * x0 / 2;
                    return (x) -> c + x * x / 2;
                }
        ));

        equations.add(new Equation(
                "y' = y / exp(x)",
                (x, y) -> y / Math.exp(x),
                (x0, y0) -> {
                    double c = y0 / Math.exp(-Math.exp(-x0));
                    return (x) -> c * Math.exp(-Math.exp(-x));
                }
        ));

        equations.add(new Equation(
                "y' = y cos(x)",
                (x, y) -> y * Math.cos(x),
                (x0, y0) -> {
                    if (y0 == 0) {
                        return (x) -> 0;
                    }
                    else {
                        double c = y0 / Math.exp(Math.sin(x0));
                        return (x) -> c * Math.exp(Math.sin(x));
                    }
                }
        ));

        return equations;
    }
}
