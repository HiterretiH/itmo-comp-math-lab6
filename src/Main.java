import equations.EquationLoader;

public class Main {
    public static void main(String... args) {
        new Application(EquationLoader.getFunctions());
    }
}
